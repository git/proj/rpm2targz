# rpm2targz

A simple tool to convert [Red Hat packages (RPMs)](https://en.wikipedia.org/wiki/RPM_Package_Manager)
to a more common [tar](https://en.wikipedia.org/wiki/Tar_%28computing%29) format.

* Homepage: https://gitweb.gentoo.org/proj/rpm2targz.git/
* Bugs/patches/etc...: https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Linux

## History

Slackware has long provided a `rpm2tgz` tool to convert between Red Hat's RPM
package format and Slackware's tgz package format.  It included a `rpm2targz`
program.  Slackware doesn't really have a dedicated project site for it since
it's part of the "base distro", so contributing changes back is difficult.

Since unpacking RPMs w/out the `rpm` program is a generally useful function,
Gentoo imported & forked it decades ago, and has since maintained a number of
improvements & updates.  Gentoo has opted to call the package `rpm2targz` to
help disambiguate and make it a little more clear that the focus is on generic
tar's rather than Slackware's tgz packages.  It's unclear how many people
picked up on this nuance ;).

Gentoo's `rpm2tgz` is specifically not compatible with Slackware's version.
Slackware's version is geared towards actually installing RPM's when your host
host distro is actually Slackware, while Gentoo's is meant to just be a generic
archive manipulation tool.  Basically we consider the "tgz" to be a short name
for "tar.gz" rather than a package format by itself.

The versioning has become desynced between the two projects.  Since Slackware
just included it in the distro, Gentoo used the overall Slackware distro version
when importing.  That's how we got:
* [rpm2targz-8.0.ebuild](https://gitweb.gentoo.org/repo/gentoo/historical.git/commit/?id=f4b34ca583b02f43962aa13fa7dc9384e2804cd8)
* [rpm2targz-9.0.ebuild](https://gitweb.gentoo.org/repo/gentoo/historical.git/commit/?id=8542b4a88931bd4fec2e29753ba3f014a19681ce)

As the Gentoo version diverged from Slackware's, we added `.0.#g` to the end to
differentiate itself (with the "g" being short for "Gentoo").  Gentoo has now
moved on to the more common date-based version style since there's no point in
staying in sync anymore.

## Slackware Readme

* http://www.slackware.com/config/packages.php
* https://pkgs.org/download/rpm2tgz

This package contains 'rpm2targz', a simple utility to convert Red Hat-style
RPM packages into standard tar.gz archives.  Converted binary packages can then
be installed/removed using the 'installpkg/removepkg' commands, or 'pkgtool'.

It's advisable to at least examine the converted package with 'less' to make
sure it won't do anything too crazy to your system.

By default, rpm2targz will attempt to use "file" to detect source RPMS, and will
put the contents into a subdirectory in the resulting package.  This may not be
portable to other operating systems -- if you're trying to run rpm2targz on an
OS that doesn't have a file that knows RPM types, and you care about this source
RPM feature, you can compile and install David Cantrell's standalone getrpmtype
utility.  The getrpmtype.tar.gz source archive can be found in Slackware's
source tree in source/a/bin/.
